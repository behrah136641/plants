import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:plants/view/screen/detail_screen.dart';
import 'package:plants/view/screen/home_screen.dart';
import 'package:plants/view/utils/pallet.dart';
import 'package:plants/view/view_model/plant_view_model.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: primaryColor));

    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: PlantsViewModel()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Main Page',
        theme: ThemeData(
          appBarTheme: const AppBarTheme(),
          primarySwatch: backGroundColor,
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => const HomeScreen(),
           '/detail': (context) => const DetailScreen(),
        },
      ),
    );
  }
}

