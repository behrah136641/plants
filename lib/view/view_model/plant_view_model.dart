import 'package:flutter/foundation.dart';
import 'package:plants/data/db/db_helper.dart';
import 'package:plants/data/model/flower.dart';

class PlantsViewModel with ChangeNotifier {
  final dbHelper = PlantsDataBaseHelper();
  List<Flower> flowers = [];

  PlantsViewModel() {
    getAllTheFlowers();
  }

  Future<void> getAllTheFlowers() async {
    try {
      flowers.clear();
      await dbHelper.init();
      var res = await dbHelper.getAllTheFlowers();
      res?.forEach((flower) {
        flowers.add(flower);
      });
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }

    Future.delayed(const Duration(seconds: 1), () {
      notifyListeners();
    });
  }
}
