import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_point_tab_bar/pointTabIndicator.dart';
import 'package:motion_tab_bar/MotionTabController.dart';
import 'package:motion_tab_bar/motiontabbar.dart';
import 'package:plants/view/utils/pallet.dart';
import 'package:plants/view/view_model/plant_view_model.dart';
import 'package:plants/view/widgets/avatar.dart';
import 'package:plants/view/widgets/flower_item.dart';
import 'package:plants/view/widgets/search_box.dart';
import 'package:plants/view/widgets/text_view.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  final tabList = ['All', 'Apartment', 'Decorative', 'Garden', 'Medicinal'];

  final PageController _pageController = PageController(
    initialPage: 1,
  );

  TextEditingController editingController = TextEditingController();

  late MotionTabController _motionTabController;
  late TabController _pointTabIndicatorController;

  @override
  void initState() {
    super.initState();
    _motionTabController = MotionTabController(initialIndex: 1, vsync: this);
    _pointTabIndicatorController =
        TabController(vsync: this, length: tabList.length);
  }

  @override
  void dispose() {
    _pageController.dispose();
    _motionTabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor,
      appBar: AppBar(
          elevation: 0.0,
          title: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                MyAvatar(
                  url: "https://media.evareh-print.com/behzad.jpg",
                ),
                Icon(
                  Icons.menu_rounded,
                  color: selectedColor,
                  size: 30.0,
                ),
              ],
            ),
          )),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            textView("The best plants", 25.0, FontWeight.bold, titleColor, 1),
            const SizedBox(
              height: 8.0,
            ),
            textView("are waiting for you !", 20.0, FontWeight.normal,
                subTitleColor, 1),
            const SizedBox(
              height: 32.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: SearchBox(
                    editingController: editingController,
                  ),
                ),
                Card(
                  elevation: 0.75,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: const SizedBox(
                    height: 60.0,
                    width: 60.0,
                    child: Icon(
                      Icons.filter_alt_sharp,
                      size: 32.0,
                      color: selectedColor,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16.0,
            ),
            TabBar(
              controller: _pointTabIndicatorController,
              isScrollable: true,
              labelColor: accentColor,
              unselectedLabelColor: unSelectedColor,
              indicator: const PointTabIndicator(
                position: PointTabIndicatorPosition.bottom,
                color: accentColor,
                insets: EdgeInsets.only(bottom: 6),
              ),
              tabs: tabList.map((item) {
                return Tab(
                  text: item,
                );
              }).toList(),
            ),
            const SizedBox(
              height: 16.0,
            ),
            Consumer<PlantsViewModel>(
                builder: (context, mPlantsViewModel, child) {

              return Expanded(
                child: mPlantsViewModel.flowers.isNotEmpty
                    ? ListView.builder(
                        itemCount: mPlantsViewModel.flowers.length,
                        itemBuilder: (_, index) =>
                            FlowerItem(mPlantsViewModel.flowers[index]),
                      )
                    : const Center(
                        child: CircularProgressIndicator(
                          color: accentColor,
                        ),
                      ),
              );
            })
          ],
        ),
      ),
      bottomNavigationBar: MotionTabBar(
        labels: const ["Cart", "Home", "Favorite"],
        initialSelectedTab: "Home",
        tabIconColor: unSelectedColor,
        tabSelectedColor: accentColor,
        onTabItemSelected: (int value) {
          setState(() {
            _motionTabController.index = value;
          });
        },
        icons: const [Icons.shopping_basket, Icons.home, Icons.favorite],
        textStyle: const TextStyle(color: selectedColor),
      ),
    );
  }
}
