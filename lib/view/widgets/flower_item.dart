import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plants/data/model/flower.dart';
import 'package:plants/view/utils/pallet.dart';
import 'package:plants/view/widgets/text_view.dart';

class FlowerItem extends StatelessWidget {
  final Flower flower;

  const FlowerItem(this.flower, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    try {
      flower.description = flower.description!.substring(0, 50) + " ...";
    } catch (e) {
      flower.description = "";
    }

    return AspectRatio(
      aspectRatio: 2 / 1,
      child: Card(
        color: Colors.white,
        elevation: 0.75,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            textView(flower.name!, 18.0, FontWeight.normal,
                                titleColor, 1),
                            textView(flower.type!, 11.0, FontWeight.normal,
                                Colors.grey, 1),
                          ],
                        ),
                        Card(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  color: Colors.grey.shade300, width: 1),
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 2),
                              child: Row(
                                children: [
                                  textView(flower.stars!, 11.0,
                                      FontWeight.normal, titleColor, 1),
                                  const Icon(
                                    Icons.star,
                                    color: Colors.orangeAccent,
                                    size: 14.0,
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
                    textView(flower.description!, 13, FontWeight.normal,
                        subTitleColor, 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            textView(
                                "\$", 11, FontWeight.normal, accentColor, 1),
                            textView(flower.price!, 12, FontWeight.normal,
                                accentColor, 1),
                          ],
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/detail');
                          },
                          child: textView(
                              "Buy", 12, FontWeight.normal, Colors.white, 1),
                          style:
                              ElevatedButton.styleFrom(primary: selectedColor),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image(
                    image: AssetImage(flower.img!),
                    width: 142,
                    height: 142,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
