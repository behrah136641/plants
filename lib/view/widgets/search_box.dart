import 'package:flutter/material.dart';
import 'package:plants/view/utils/pallet.dart';

class SearchBox extends StatelessWidget {
  final TextEditingController editingController;

  const SearchBox({required this.editingController, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.75,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: TextField(
        controller: editingController,
        onChanged: (value) {},
        cursorColor: accentColor,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          border: myInputBorder(),
          enabledBorder: myInputBorder(),
          focusedBorder: myFocusBorder(),
          hintText: "Search ...",
          hintStyle: const TextStyle(
            color: Colors.black12,
          ),
          prefixIcon: const Icon(
            Icons.search,
            size: 32.0,
          ),
        ),
      ),
    );
  }

  OutlineInputBorder myInputBorder() {
    return const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        borderSide: BorderSide(
          color: Colors.white30,
          width: 3,
        ));
  }

  OutlineInputBorder myFocusBorder() {
    return const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        borderSide: BorderSide(
          color: accentColor,
          width: 2,
        ));
  }
}
