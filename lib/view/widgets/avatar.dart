import 'package:flutter/material.dart';
import 'package:loading_gifs/loading_gifs.dart';

class MyAvatar extends StatelessWidget {
  final double size = 50.0;
  final String? url;

  const MyAvatar({this.url, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Card(
          elevation: 0,
          color: const Color(0xFF8BF583),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(size / 2),
          ),
          child: ClipOval(
            child: SizedBox.fromSize(
                size: Size.fromRadius(size / 2), // Image radius
                child: url != null
                    ? FadeInImage.assetNetwork(
                        placeholder: cupertinoActivityIndicator,
                        fit: BoxFit.cover,
                        image: url!,
                        placeholderScale: 10)
                    : Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Image.asset(
                          "assets/images/ic_user.png",
                        ),
                      )),
          ),
        )
      ],
    );
  }
}
