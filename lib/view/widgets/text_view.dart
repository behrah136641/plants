import 'package:flutter/cupertino.dart';

Widget textView(String text, double fontSize, FontWeight fontWeight,
    Color color, int maxLines) {
  return Text(text,
      maxLines: maxLines,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontFamily: 'SfproDisplayRegular',
        color: color,
      ));
}
