import 'package:flutter/material.dart';

Map<int, Color> color = {
  50: const Color.fromRGBO(246, 246, 246, .1),
  100: const Color.fromRGBO(246, 246, 246, .2),
  200: const Color.fromRGBO(246, 246, 246, .3),
  300: const Color.fromRGBO(246, 246, 246, .4),
  400: const Color.fromRGBO(246, 246, 246, .5),
  500: const Color.fromRGBO(246, 246, 246, .6),
  600: const Color.fromRGBO(246, 246, 246, .7),
  700: const Color.fromRGBO(246, 246, 246, .8),
  800: const Color.fromRGBO(246, 246, 246, .9),
  900: const Color.fromRGBO(246, 246, 246, 1),
};
MaterialColor backGroundColor = MaterialColor(0xFFF6F6F6, color);
const primaryColor=Color(0xFFF6F6F6);
const titleColor=Color(0xFF344155);
const subTitleColor=Color.fromRGBO(121, 121, 121, 1);
const unSelectedColor=Color.fromRGBO(190, 188, 200, 1);
const selectedColor=Color(0xFF344155);
const accentColor=Color.fromRGBO(140, 170, 90, 1);

