import 'package:flutter/services.dart';
import 'package:plants/data/model/flower.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'dart:io' as io;


class PlantsDataBaseHelper {
  Database? _db;

  Future<void> init() async {
    io.Directory applicationPlants =
    await getApplicationDocumentsDirectory();

    String dbPlants =
    path.join(applicationPlants.path, "plantsDb.db");

    bool dbExistsPlants = await io.File(dbPlants).exists();

    if (!dbExistsPlants) {

      ByteData data = await rootBundle.load("assets/db/plants.db");
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await io.File(dbPlants).writeAsBytes(bytes, flush: true);
    }

    _db = await openDatabase(dbPlants);
  }

  Future<List<Flower>?> getAllTheFlowers() async {
    if (_db == null) {
      throw "bd is not initiated, initiate using [init(db)] function";
    }
    List<Map<String, dynamic>>? flowers;

    await _db!.transaction((txn) async {
      flowers = await txn.query(
        "flowers",
        columns: [
          "id",
          "name",
          "type",
          "description",
          "price",
          "stars",
          "img",
        ],
      );
    });

    return flowers?.map((e) => Flower.fromJson(e)).toList();
  }
}
