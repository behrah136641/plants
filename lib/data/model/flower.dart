class Flower {
  Flower({
    this.id,
    this.name,
    this.type,
    this.description,
    this.price,
    this.stars,
    this.img,
  });

  factory Flower.fromJson(Map<String, dynamic> json) => Flower(
    id : json['id'],
    name : json['name'],
    type : json['type'],
    description : json['description'],
    price : json['price'],
    stars : json['stars'],
    img : json['img'],
  );

  int? id;
  String? name;
  String? type;
  String? description;
  String? price;
  String? stars;
  String? img;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['type'] = type;
    map['description'] = description;
    map['price'] = price;
    map['stars'] = stars;
    map['img'] = img;
    return map;
  }
}
